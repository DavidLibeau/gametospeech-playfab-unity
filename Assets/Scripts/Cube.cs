﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cube : MonoBehaviour
{
    public GameObject playFab; 
    private void OnTriggerEnter(Collider collider)
    {
        if(collider.gameObject.name == "Player"){
            playFab.GetComponent<PlayFabLogin>().OnAudioDescriptionEvent("collide", this.gameObject.name);
        }
    }
}
