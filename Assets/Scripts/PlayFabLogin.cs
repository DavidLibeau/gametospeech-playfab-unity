﻿using System.Collections;
using System.Collections.Generic;
using PlayFab;
using PlayFab.ClientModels;
using UnityEngine;

public class PlayFabLogin : MonoBehaviour
{
    public string gametospeechAccessToken;
    public string gametospeechChannel;
    public void Start()
    {
        if (string.IsNullOrEmpty(PlayFabSettings.staticSettings.TitleId)){
            /*
            Please change the titleId below to your own titleId from PlayFab Game Manager.
            If you have already set the value in the Editor Extensions, this can be skipped.
            */
            PlayFabSettings.staticSettings.TitleId = "42";
        }
        var request = new LoginWithCustomIDRequest { CustomId = "GettingStartedGuide", CreateAccount = true};
        PlayFabClientAPI.LoginWithCustomID(request, OnLoginSuccess, OnLoginFailure);
    }

    private void OnLoginSuccess(LoginResult result)
    {
        Debug.Log("Logged to PlayFab");
    }

    private void OnLoginFailure(PlayFabError error)
    {
        Debug.LogError("Error loggin to PlayFab");
        Debug.LogError(error.GenerateErrorReport());
    }

    public void OnAudioDescriptionEvent(string type, string value) {
        if(this.gametospeechAccessToken.Length > 0 && this.gametospeechChannel.Length > 0){
            PlayFabClientAPI.WritePlayerEvent(new WriteClientPlayerEventRequest() {
                Body = new Dictionary<string, object>() {
                    { "access_token", this.gametospeechAccessToken},
                    { "channel", this.gametospeechChannel},
                    { "type", type },
                    { "value", value }
                },
                EventName = "audio_description"
            },
            result => Debug.Log("audio_description event sent"),
            error => Debug.LogError(error.GenerateErrorReport()));
        }
    }
}